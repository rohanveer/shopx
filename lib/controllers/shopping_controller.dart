// ignore_for_file: prefer_collection_literals, deprecated_member_use

import 'package:get/state_manager.dart';

import '../models/product.dart';

class ShoppingController extends GetxController {
  var products = <Product>[].obs;
  /*
  in getx we can update user interface with three different methods
  get builder (you have to update your data manually | do not write obs)
  get x
  obx

  when to use what?
  >get builder: it takes mannual work and does not allow obs
  >get x : it takes type of controller and provides it, and uses obs
  >obx : it does not provide builder, we have to initialize controller and uses obs 
   */

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(const Duration(seconds: 1));
    var productsResult = [
      Product(
        id: 1,
        productName: 'FirstPod',
        productImage: 'adb',
        productDescription: 'something something',
        price: 150,
      ),
      Product(
        id: 1,
        productName: 'SecondPod',
        productImage: 'adb',
        productDescription: 'something something',
        price: 200,
      ),
      Product(
        id: 1,
        productName: 'ThirdPod',
        productImage: 'adb',
        productDescription: 'something something',
        price: 999,
      ),
    ];
    products.value = productsResult;
  }
}
