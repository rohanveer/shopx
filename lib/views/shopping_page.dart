import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tutorial_getx/controllers/cart_controller.dart';
import 'package:tutorial_getx/controllers/shopping_controller.dart';

class ShoppingPage extends StatelessWidget {
  final shoppingController = Get.put(ShoppingController());
  final cartController = Get.put(CartController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF44111),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        backgroundColor: Colors.amberAccent,
        label: GetX<CartController>(
          builder: (controller) {
            return Text(
              controller.count.toString(),
              style: const TextStyle(color: Colors.black, fontSize: 20),
            );
          },
        ),
        icon: const Icon(
          Icons.add_shopping_cart_rounded,
          color: Colors.black,
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: GetX<ShoppingController>(
                builder: (controller) {
                  return ListView.builder(
                    itemCount: controller.products.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: const EdgeInsets.all(12.0),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        controller.products[index].productName,
                                        style: const TextStyle(fontSize: 24),
                                      ),
                                      Text(
                                        controller
                                            .products[index].productDescription,
                                        style: const TextStyle(fontSize: 24),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    controller.products[index].price.toString(),
                                    style: const TextStyle(fontSize: 24),
                                  ),
                                ],
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  cartController
                                      .addToCart(controller.products[index]);
                                },
                                child: const Text('add to cart'),
                              ),
                              Obx(
                                () => IconButton(
                                  onPressed: () {
                                    controller.products[index].isFavourite
                                        .toggle();
                                  },
                                  icon: controller
                                          .products[index].isFavourite.value
                                      ? const Icon(Icons.circle)
                                      : const Icon(Icons.circle_outlined),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
            // GetBuilder<CartController>(
            //   builder: (controller) {
            //     return Text(
            //       '${controller.testAmount}',
            //       style: const TextStyle(fontSize: 25),
            //     );
            //   },
            // ),
            // GetX<CartController>(
            //   builder: (controller) {
            //     return Text(
            //       'Total Amount : \$ ${controller.totalPrice}',
            //       style: const TextStyle(fontSize: 25),
            //     );
            //   },
            // ),
            Obx(() => Text(
                  'Total Amount : ${cartController.totalPrice}',
                  style: const TextStyle(fontSize: 25),
                )),
            const SizedBox(height: 70),
          ],
        ),
      ),
    );
  }
}
